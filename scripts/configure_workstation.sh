#!/usr/bin/env bash
# This script configures the workstation in a DO280 classroom for use in
# the Openshift Operator workshop

# install Ansible kubernetes modules with ansible galaxy
ansible-galaxy collection install community.kubernetes

# install python modules
sudo pip3 install openshift pyyaml kubernetes

# install make
sudo yum install -y make